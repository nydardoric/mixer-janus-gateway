var Helpers = require('../../helpers');
var Plugin = require('../../plugin');
var MediaStreamPlugin = require('../media-stream-plugin');

function MixerFTLPlugin() {
  MixerFTLPlugin.super_.apply(this, arguments);
}

MixerFTLPlugin.NAME = 'janus.plugin.plex';
Helpers.inherits(MixerFTLPlugin, MediaStreamPlugin);
Plugin.register(MixerFTLPlugin.NAME, MixerFTLPlugin);

/**
 * @see https://janus.conf.meetecho.com/docs/janus__streaming_8c.html
 * @param {number} id
 * @param {Object} [options]
 * @param {string} [options.type]
 * @param {string} [options.secret]
 * @param {string} [options.pin]
 * @param {boolean} [options.permanent]
 * @param {string} [options.description]
 * @param {boolean} [options.is_private]
 * @param {string} [options.filename]
 * @param {boolean} [options.audio]
 * @param {boolean} [options.video]
 * @param {number} [options.audioport]
 * @param {string} [options.audiomcast]
 * @param {number} [options.audiopt]
 * @param {string} [options.audiortpmap]
 * @param {string} [options.audiofmtp]
 * @param {number} [options.videoport]
 * @param {string} [options.videomcast]
 * @param {number} [options.videopt]
 * @param {string} [options.videortpmap]
 * @param {string} [options.videofmtp]
 * @param {boolean} [options.videobufferkf]
 * @param {string} [options.url]
 * @returns {Promise}
 * @fulfilled {JanusPluginMessage} response
 */
MixerFTLPlugin.prototype.create = function(id, options) {
  return this._create(id, options);
};

/**
 * @param {number} id
 * @param {Object} [options]
 * @param {string} [options.secret]
 * @param {boolean} [options.permanent]
 * @returns {Promise}
 * @fulfilled {JanusPluginMessage} response
 */
MixerFTLPlugin.prototype.destroy = function(id, options) {
  return this._destroy(id, options);
};

/**
 * @returns {Promise}
 * @fulfilled {JanusPluginMessage} response
 */
MixerFTLPlugin.prototype.list = function(options) {
  return this._list(options);
};

/**
 * @param {number} id
 * @param {Object} [watchOptions]
 * @param {string} [watchOptions.pin]
 * @param {Object} [answerOptions] {@link createAnswer}
 * @returns {Promise}
 * @fulfilled {JanusPluginMessage} response
 */
MixerFTLPlugin.prototype.watch = function(id, watchOptions, answerOptions) {
  return this._watch(id, watchOptions, answerOptions);
};

/**
 * @param {RTCSessionDescription} [jsep]
 * @returns {Promise}
 * @fulfilled {JanusPluginMessage} response
 */
MixerFTLPlugin.prototype.start = function(jsep) {
  return this._start(jsep);
};

/**
 * @returns {Promise}
 * @fulfilled {JanusPluginMessage} response
 */
MixerFTLPlugin.prototype.stop = function() {
  return this._stop();
};

/**
 * @returns {Promise}
 * @fulfilled {JanusPluginMessage} response
 */
MixerFTLPlugin.prototype.pause = function() {
  return this._pause();
};

/**
 * @param {number} mountpointId
 * @param {Object} [options] {@link watch}
 * @returns {Promise}
 * @fulfilled {JanusPluginMessage} response
 */
MixerFTLPlugin.prototype.switch = function(mountpointId, options) {
  return this._switch(mountpointId, options);
};

/**
 * @param {number} mountpointId
 * @param {Object} [options]
 * @param {string} [options.secret]
 * @returns {Promise}
 * @fulfilled {JanusPluginMessage} response
 */
MixerFTLPlugin.prototype.enable = function(mountpointId, options) {
  var body = Helpers.extend(
    {
      request: 'enable',
      id: mountpointId,
    },
    options,
  );
  return this.sendWithTransaction({ body: body });
};

/**
 * @param {number} mountpointId
 * @param {Object} [options]
 * @param {string} [options.secret]
 * @returns {Promise}
 * @fulfilled {JanusPluginMessage} response
 */
MixerFTLPlugin.prototype.disable = function(mountpointId, options) {
  var body = Helpers.extend(
    {
      request: 'disable',
      id: mountpointId,
    },
    options,
  );
  return this.sendWithTransaction({ body: body }).then(
    function() {
      if (this.hasCurrentEntity(mountpointId)) {
        this.resetCurrentEntity();
      }
    }.bind(this),
  );
};

/**
 * @param {number} mountpointId
 * @param {Object} [options]
 * @param {string} [options.secret]
 * @param {string} [options.action]
 * @param {string} [options.audio]
 * @param {string} [options.video]
 * @returns {Promise}
 * @fulfilled {JanusPluginMessage} response
 */
MixerFTLPlugin.prototype.recording = function(mountpointId, options) {
  var body = Helpers.extend(
    {
      request: 'recording',
      id: mountpointId,
    },
    options,
  );
  return this.sendWithTransaction({ body: body });
};

/**
 * @inheritDoc
 */
MixerFTLPlugin.prototype.getResponseAlias = function() {
  return 'streaming';
};

module.exports = MixerFTLPlugin;
